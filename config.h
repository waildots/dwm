/* See LICENSE file for copyright and license details. */

/* Constants */
#define TERMINAL "st"
#define TERMCLASS "St"
#define STERMINAL "alacritty"
#define STERMCLASS "Alacritty"

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 0;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "JetBrains Mono:size=10", "FontAwesome:size=13", "Font Awesome 5 Free:pixelsize=13" , "Font Awesome 5 Free Solid:pixelsize=13", "Font Awesome 5 Brands:pixelsize=13" };
static const char dmenufont[]       = "monospace:size=11";
static const char col_gray1[]       = "#20252e";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#f7f5f5";
static const char col_gray4[]       = "#f5f5f5";
static const char col_cyan[]        = "#444e63";
static const char col_border[]      = "#598f9e";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_border  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     iscentered      isfloating   monitor    scratch key */
	{ "Gimp",     NULL,       NULL,       0,               0,                 1,           -1,        0  },
	{ "mpv",     NULL,       NULL,       0,                0,               1,           -1,        0  },
	{ NULL,       NULL,   "scratchpad",   0,               1,                 1,           -1,       't' },
	{ NULL,       NULL,   "spvol",   0,                    1,                  1,           -1,       'v' },
	{ NULL,       NULL,   "spmusic",   0,                  1,                 1,           -1,       'm' },
	{ NULL,       NULL,   "spwmemo",   0,                  1,                 1,           -1,       'w' },
	{ NULL,       NULL,   "sppod",   0,                    1,                  1,           -1,       'p' },
	{ NULL,       NULL,   "spnb",   0,                     1,                     1,           -1,       'n' },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MD Mod4Mask
#define CTRL ControlMask
#define ALT Mod1Mask
#define SHIFT ShiftMask

#define TAGKEYS(KEY,TAG) \
	{ MD,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MD|CTRL,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MD|SHIFT,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MD|CTRL|SHIFT, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MD,ACTION) \
	{ MD, XK_j,     ACTION##stack, {.i = INC(+1) } }, \
	{ MD, XK_k,     ACTION##stack, {.i = INC(-1) } }, \
	/* { MD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \ */
	/*{ MD, XK_q,     ACTION##stack, {.i = 0 } }, \ */
	/*{ MD, XK_a,     ACTION##stack, {.i = 1 } }, \ */
	/*{ MD, XK_z,     ACTION##stack, {.i = 2 } }, \ */
	/*{ MD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }


/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]           = { "rofi", "-show", "drun", "-show-icons", NULL };
static const char *stermcmd[]           = { STERMINAL, "-e", "tmux", "new-session", "-A", "-s", "0", NULL };
static const char *termcmd[]            = { STERMINAL, NULL };
static const char *vifm[]               = { STERMINAL, "-e", "vifm", NULL };
static const char *htop[]               = { STERMINAL, "-e", "htop", NULL };
static const char *fscreenshot[]        = { "fscreenshot", NULL };
static const char *brave[]              = { "brave", NULL };
static const char *doom[]               = { "emacs", NULL };
static const char *myemacs[]            = { "/home/wn/myemacs/run-emacs.sh", NULL };
static const char *regionss[]           = { "screenshot", NULL };
static const char *maimpick[]           = { "rofi-maim", NULL };
static const char *pcmanfm[]            = { "pcmanfm", NULL };
static const char *scrcpy[]             = { "scrcpy", NULL };
static const char *slock[]              = { "slock", NULL };
static const char *rofiwinswitcher[]    = { "rofi", "-show", "window", "-show-icons", NULL };
static const char *emojis[]             = { "rofi-unicode", NULL };
static const char *spotify[]            = { "env", "LD_PRELOAD=/usr/lib/spotify-adblock.so", "spotify", "%U", NULL };
static const char *copyq[]              = { "copyq", "menu", NULL };
static const char *sysact[]             = { "rofi-powermenu", NULL };
static const char *picomtoggle[]        = { "picom-toggle", NULL };
static const char *dmenumount[]         = { "rofi-mount", NULL };
static const char *dmenuumount[]        = { "rofi-umount", NULL };
static const char *mpctoggle[]          = { "mpc", "toggle", NULL };
static const char *mpcprev[]            = { "mpc", "prev", NULL };
static const char *mpcnext[]            = { "mpc", "next", NULL };
static const char *mpcplaylist[]        = { "rofi-mpd-playlist", NULL };
static const char *mpcqueue[]           = { "rofi-mpd-queue", NULL };
static const char *rofivirtualbox[]     = { "rofi-virtualbox", NULL };
static const char *topleft[]            = { "wm-corners", "topleft", NULL };
static const char *topright[]           = { "wm-corners", "topright", NULL };
static const char *bottomleft[]         = { "wm-corners", "bottomleft", NULL };
static const char *bottomright[]        = { "wm-corners", "bottomright", NULL };

/*First arg only serves to match against key in rules*/
static const char *spcmd1[] = {"t", TERMINAL, "-t", "scratchpad", "-g", "120x34", NULL };
static const char *spcmd2[] = {"v", TERMINAL, "-t", "spvol", "-g", "112x25", "-e", "pulsemixer", NULL };
static const char *spcmd3[] = {"m", TERMINAL, "-t", "spmusic", "-g", "108x30", "-e", "ncmpcpp", NULL };
static const char *spcmd4[] = {"w", TERMINAL, "-t", "spwmemo", "-g", "128x38", "-e", "nvim", "/home/wn/pc/txt/productivity/working-mem.md", NULL };

#include <X11/XF86keysym.h>
#include "shiftview.c"

static Key keys[] = {
	/* modifier                     key                 function            argument */
    TAGKEYS(                        XK_1,                                   0)
	TAGKEYS(                        XK_2,                                   1)
	TAGKEYS(                        XK_3,                                   2)
	TAGKEYS(                        XK_4,                                   3)
	TAGKEYS(                        XK_5,                                   4)
	TAGKEYS(                        XK_6,                                   5)
	TAGKEYS(                        XK_7,                                   6)
	TAGKEYS(                        XK_8,                                   7)
	TAGKEYS(                        XK_9,                                   8)

// MOD Launchers ---------------------------------------------------------------------------------------------------
	{ MD,                           XK_b,               togglebar,          {0} },
	{ MD,                           XK_q,               incnmaster,         {.i = +1 } },
	{ MD,                           XK_d,               incnmaster,         {.i = -1 } },
	{ MD,                           XK_h,               setmfact,           {.f = -0.05} },
	{ MD,                           XK_l,               setmfact,           {.f = +0.05} },
	{ MD,                           XK_g,               zoom,               {0} },
	{ MD,                           XK_Tab,             view,               {0} },
	{ MD,                           XK_t,               setlayout,          {.v = &layouts[0]} },
	{ MD,                           XK_f,               setlayout,          {.v = &layouts[1]} },
	{ MD,                           XK_m,               setlayout,          {.v = &layouts[2]} },
	{ MD,                           XK_space,           setlayout,          {0} },
	{ MD,                           XK_s,               togglesticky,       {0} },
	{ MD,                           XK_comma,           focusmon,           {.i = -1 } },
	{ MD,                           XK_period,          focusmon,           {.i = +1 } },
	{ MD,                           XK_0,               view,               {.ui = ~0 } },
	{ MD,                           XK_minus,           setgaps,            {.i = -1 } },
	{ MD,                           XK_equal,           setgaps,            {.i = +1 } },
	{ MD,                           XK_Return,          spawn,              {.v = stermcmd } },
	{ MD,			                XK_w,	            spawn,			    {.v = brave } },
	{ MD,			                XK_i,	            spawn,			    {.v = myemacs } },
	{ MD,		        	        XK_e,	            spawn,			    {.v = vifm } },
	{ MD,			                XK_p,	            spawn,			    {.v = dmenucmd } },
	{ MD,			                XK_semicolon,	    spawn,			    {.v = emojis } },
	{ MD,			                XK_v,	            spawn,			    {.v = copyq } },
	{ MD,			                XK_Print,	        spawn,			    {.v = maimpick } },
	{ 0,				            XK_Print,	        spawn,			    {.v = regionss } },
	STACKKEYS(MD,                                       focus)

// MOD + Shift Launchers -------------------------------------------------------------------------------------------
	{ MD|SHIFT,                     XK_c,               killclient,         {0} },
	{ MD|SHIFT,                     XK_space,           togglefloating,     {0} },
	{ MD|SHIFT,                     XK_f,               togglefullscr,      {0} },
	{ MD|SHIFT,                     XK_0,               tag,                {.ui = ~0 } },
	{ MD|SHIFT,                     XK_comma,           tagmon,             {.i = -1 } },
	{ MD|SHIFT,                     XK_period,          tagmon,             {.i = +1 } },
	{ MD|SHIFT,                     XK_equal,           setgaps,            {.i = 0  } },
	{ MD|SHIFT,                     XK_BackSpace,       quit,               {0} },
	{ MD|SHIFT,                     XK_Return,          spawn,              {.v = termcmd } },
	{ MD|SHIFT,	                    XK_e,	            spawn,			    {.v = pcmanfm } },
	{ MD|SHIFT,	                    XK_i,	            spawn,			    {.v = doom } },
	{ MD|SHIFT,			            XK_Print,	        spawn,			    {.v = fscreenshot } },
	{ MD|SHIFT,		                XK_h,	            spawn,		        {.v = htop } },
	{ MD|SHIFT,		                XK_l,	        	spawn,			    {.v = sysact } },
	STACKKEYS(MD|SHIFT,                                 push)

// MOD + Control Launchers -------------------------------------------------------------------------------------------
	{ MD|CTRL,		                XK_Left,	        shiftview,		    { .i = -1 } },
	{ MD|CTRL,		                XK_Right,	        shiftview,		    { .i = +1 } },
	{ MD|CTRL,		                XK_u,	        	spawn,			    {.v = picomtoggle } },
	{ MD|CTRL,		                XK_q,	        	spawn,			    {.v = topleft } },
	{ MD|CTRL,		                XK_p,	        	spawn,			    {.v = topright } },
	{ MD|CTRL,		                XK_a,	        	spawn,			    {.v = bottomleft } },
	{ MD|CTRL,		                XK_semicolon,	    spawn,			    {.v = bottomright } },
	{ MD|CTRL,		                XK_m,	        	spawn,			    {.v = dmenumount } },
	{ MD|CTRL|SHIFT,		        XK_m,	        	spawn,			    {.v = dmenuumount } },

// MOD + Alt Launchers ---------------------------------------------------------------------------------------------
	{ MD|ALT,	   	                XK_a,	            spawn,			    {.v = scrcpy } },
	{ MD|ALT,		                XK_b,	            spawn,			    {.v = slock } },
	{ MD|ALT,		                XK_s,	            spawn,			    {.v = spotify } },
	{ MD|ALT,	                    XK_p,	            spawn,			    {.v = mpctoggle } },
	{ MD|ALT,	                    XK_Left,	        spawn,			    {.v = mpcprev } },
	{ MD|ALT,	                    XK_Right,	        spawn,			    {.v = mpcnext } },
	{ MD|ALT,	                    XK_r,	            spawn,			    {.v = mpcplaylist } },
	{ MD|ALT,	                    XK_d,	            spawn,			    {.v = mpcqueue } },
	{ MD|ALT,	                    XK_v,	            spawn,			    {.v = rofivirtualbox } },

// ALT Launchers ----------------------------------------------------------------------------------------------------
	{ ALT,			                XK_Tab,	            spawn,			    {.v = rofiwinswitcher } },
	{ ALT,                          XK_t,               togglescratch,      {.v = spcmd1 } },
	{ ALT,                          XK_s,               togglescratch,      {.v = spcmd2 } },
	{ ALT,                          XK_m,               togglescratch,      {.v = spcmd3 } },
	{ ALT,                          XK_w,               togglescratch,      {.v = spcmd4 } },
	{ ALT,			                XK_space,	        spawn,			    SHCMD("kill -37 $(pidof dwmblocks)") },

// Fn Keys -----------------------------------------------------------------------------------------------------------
	{ 0, 				XF86XK_AudioMute,		        spawn,			    SHCMD("pamixer -t; kill -36 $(pidof dwmblocks)") },
	{ 0, 				XF86XK_AudioRaiseVolume,	    spawn,			    SHCMD("pamixer --allow-boost -i 3; kill -36 $(pidof dwmblocks)") },
	{ 0, 				XF86XK_AudioLowerVolume,	    spawn,			    SHCMD("pamixer --allow-boost -d 3; kill -36 $(pidof dwmblocks)") },
	{ 0, 				XF86XK_AudioMicMute,	        spawn,			    SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0, 				XF86XK_MonBrightnessUp,	        spawn,			    SHCMD("xbacklight -inc 4") },
	{ 0, 				XF86XK_MonBrightnessDown,	    spawn,			    SHCMD("xbacklight -dec 4") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkClientWin,         MD,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MD,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MD,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MD,         Button1,        tag,            {0} },
	{ ClkTagBar,            MD,         Button3,        toggletag,      {0} },
};
