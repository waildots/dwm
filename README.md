dwm - dynamic window manager

patches :

- actualfullscreen : fullscreen with mod+shift+f
- attachasideandbelow : 
- center : center rule (better than the alwayscenter)
- focusonnetactive : focuses urgent window (useful to use rofi's window swticher"
- fullgaps : can add gaps between windows (have them set to 0)
- namedscratchpads : the best patch for scratchpad (no conflict with shiftview)
- pertag : layout per tag
- stacker : controlling window positions ...
- statuscmd : integration with dwmblocks
- sticky : can make windows appear in all tags
- systray : add a tray
